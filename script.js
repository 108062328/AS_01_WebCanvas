const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const picker = document.getElementById('color-block');
const ptx = picker.getContext('2d');
const RGBStrip = document.getElementById('color-strip');
const rtx = RGBStrip.getContext('2d');


let statePointer = 0;
let maxState = 0;
let lineWidth = 10;
let draw = false;
let pick = false;
let typing = false;
let x1, x2, y1, y2 = 0;
let ameIndex = 0;
let pickedColor = "rgba(65,164,255,1)";

let fontSize = "20px";
let fontFamily = "Arial";
/*

let cursors = {
  "pencil": `'\f4cb'`,
  "fonts": `"\\f3da"`,
  "eraser": `'"f331"`,
  "circle": `\f28a`,
  "square": `\f584`,
  "triangle": `\\f5e5`,
  "arrow-clockwise": `\\f116`,
  "arrow-counterclockwise": `\\f117`,
  "download": `\\f29b`,
  "upload": `\\f2c0`,
  "trash": `\\f5de`,
}
*/

function setcursor(tool){
  if(tool != "ame"){
    canvas.style.cursor = `url(` + `assets/` + tool + `.svg), auto`;
  }else{
    
    canvas.style.cursor = `url(` + `assets/` + "pencil" + `.svg), auto`;

    console.log(cu);
    canvas.style.cursor = cu;
    
  }
  
}

function setgif(gif){
  
  let gifs = {
     "pencil": "https://walfiegif.files.wordpress.com/2020/11/out-transparent-15.gif?w=350",
     "eraser": "https://walfiegif.files.wordpress.com/2020/11/out-transparent-16.gif?w=371",
     "fonts": "https://walfiegif.files.wordpress.com/2020/11/out-transparent-7.gif?w=371",
     "circle":"https://walfiegif.files.wordpress.com/2020/12/out-transparent-4.gif?w=371",
     "square":"https://walfiegif.files.wordpress.com/2020/11/out-transparent-24.gif?w=371",
     "triangle":"https://walfiegif.files.wordpress.com/2021/01/out-transparent-3.gif?w=260",
     "undo":"https://walfiegif.files.wordpress.com/2020/11/out-transparent-8.gif?w=371",
     "redo":"https://walfiegif.files.wordpress.com/2020/12/out-transparent-14.gif?w=371",
     "upload":"https://walfiegif.files.wordpress.com/2021/02/out-transparent-1.gif?w=371",
    "download":"https://walfiegif.files.wordpress.com/2020/11/out-transparent-39.gif?w=371",
    "trash" :"https://walfiegif.files.wordpress.com/2020/12/out-transparent-6.gif?w=371",
    "ame":"https://walfiegif.files.wordpress.com/2020/11/out-transparent-36.gif?w=371",
   }
  
  //document.body.style.backgroundImage = `url("` +gifs[gif] + `")`
  
  document.getElementById('col2').style.backgroundImage = `url("` + gifs[gif] + `")`
}


function statePush(){
  let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);
  
  maxState = statePointer;
  ++maxState;
  statePointer = maxState;
}

let ames = [];
window.onload = function(){
  ctx.lineWidth = lineWidth;
  ctx.lineCap = 'round';
  
  picker.style.cursor = "crosshair";
  
  fillPicker();
  fillRGBStrip();
  
  statePush();
  
  for(let i = 0; i<= 18; ++i){
    //let a = 
    ames[i] = new Image();
    ames[i].src = `assets/gif/frame_`+ i + `_delay-0.06s.gif`;
 
  }
  
  //canvas.style.cursor = `url("http://simpleicon.com/wp-content/uploads/pencil-256x256.png"), auto`
  
};



let tempCanvas;
// Canvas http://www.ezonesoft.com.tw/JavaScript/WebPainter.htm
canvas.addEventListener("mousedown", (e) => {
  
  //let dataURL = 'http://simpleicon.com/wp-content/uploads/pencil.png';
  //let str = "url(\'" + dataURL + "\'), auto;";
  //canvas.style.cursor = str;
  //console.log(str);
  x1 = e.offsetX;
  y1 = e.offsetY;
  
  ctx.fillStyle = pickedColor;
  ctx.strokeStyle = pickedColor;
  console.log("mouse down");
  let tool = document.querySelector('input[name="btnradio"]:checked').id;
  
  if(tool == "circle" || tool == "square" || tool == "triangle"){
     tempCanvas = ctx.getImageData(0, 0, canvas.width, canvas.height);
  }else if(tool == "fonts" && (!typing)){
    console.log("fonts");
    typing = true;
    
    let textArea = document.createElement('input');
    textArea.type = 'text';
    textArea.style.position = 'fixed';
    textArea.style.left = e.clientX + "px";
    textArea.style.top = e.clientY + "px";
    document.body.appendChild(textArea);
    //textArea.focus();
    textArea.onkeydown = (e) => {
      if (e.keyCode === 13) {
          typing = false;
          //ctx.textBaseline = 'top';
          //ctx.textAlign = 'left';
          ctx.font = fontSize + " " + fontFamily;

          ctx.fillText(textArea.value, x1, y1);
          document.body.removeChild(textArea);
      }
    }
  }else if(tool == "ame"){

    ctx.beginPath();
    ctx.moveTo(x1, y1);

    ++ameIndex;
    if(ameIndex >= 19) ameIndex = 0;
    console.log(typeof(ames[ameIndex]));
    ctx.drawImage(ames[ameIndex], x1, y1, dWidth = "75", dHeight = "75");


  }else{
    ctx.beginPath();
  }
  
  draw = true;
  
  ctx.moveTo(x1, y1);
  ctx.strokeStyle = pickedColor;
});

canvas.addEventListener("mousemove", (e) => {
  
  if(draw){
    x2 = e.offsetX;
    y2 = e.offsetY;
    console.log("mouse move : " + x2 + " " + y2);
    
    let tool = document.querySelector('input[name="btnradio"]:checked').id;
    
    
    if(tool == "pencil"){
      ctx.lineTo(x2, y2);
      ctx.stroke();
      
      
  
      
      
      
    }else if(tool == "eraser"){
      ctx.moveTo(x2, y2);
      ctx.clearRect(e.offsetX, e.offsetY, lineWidth, lineWidth);  // ctx.clearRect(x, y, width, height);
      
    }else if(tool == "ame"){
++ameIndex;
    if(ameIndex >= 19) ameIndex = 0;
    ctx.drawImage(ames[ameIndex], x2, y2, dWidth = "75", dHeight = "75");
      
    }else if(tool == "circle"){
      
      let radius = Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2));
      
      ctx.putImageData(tempCanvas, 0, 0);
      
      ctx.beginPath();
 			ctx.arc(x1, y1, radius, 0, 2*Math.PI, true);
      
      ctx.stroke();
      
      
      
    }else if(tool == "square"){
      
      ctx.putImageData(tempCanvas, 0, 0);
      ctx.beginPath();
      ctx.rect(x1, y1, x2-x1, y2-y1);
      ctx.stroke();
      
      
    }else if(tool == "triangle"){
      ctx.putImageData(tempCanvas, 0, 0);
      ctx.beginPath();
      ctx.moveTo(x1, y1);
      //ctx.lineTo(x1, y1);
      ctx.lineTo(x2,y2);
      ctx.lineTo(x1 - x2 + x1, y2);
      ctx.closePath();
      ctx.stroke();
    }
    
  }
});

canvas.addEventListener("mouseup", (e) => {
  
  let tool = document.querySelector('input[name="btnradio"]:checked').id;

  draw = false;
  statePush();
  
}, false);

canvas.addEventListener("mouseout", (e) => {
  draw = false;
}, false);

canvas.addEventListener("mouseenter", (e) => {
  let tool = document.querySelector('input[name="btnradio"]:checked').id;
  setcursor(tool);
}, false);


document.getElementById('btn-group').addEventListener("click", (e) =>{
  
  
  
  
   let tool = document.querySelector('input[name="btnradio"]:checked').id;
   
  setgif(tool);
    
  
});

document.getElementById('download').addEventListener("click", (e) =>{
  setgif("download");
  // https://stackoverflow.com/a/50300880/10242733
  let link = document.createElement('a');
  link.download = 'png.png';
  link.href = document.getElementById('canvas').toDataURL()
  link.click();
  
}, false);

document.getElementById('upload').addEventListener("change", (e) =>{
  setgif("upload");
  // https://stackoverflow.com/a/51760964/10242733
  let reader = new FileReader();
  
    reader.onload = (event) => {
        let img = new Image();
        img.src = event.target.result || URL.createObjectURL(this.files[0]);
        img.onload = () => ctx.drawImage(img, 0, 0);
        img.onerror = () => console.log("upload failed");
    }
    reader.readAsDataURL(e.target.files[0]);
  
}, false);


document.getElementById('undo').addEventListener("click", (e) =>{
  
  setgif("undo");
  if(statePointer > 0){
    --statePointer;
    window.history.back();
  } 
   
}, false);

document.getElementById('redo').addEventListener("click", (e) =>{
  
  setgif("redo");
  if(statePointer < maxState){
    ++statePointer;
    window.history.forward();
  }
  
}, false);



document.getElementById('trash').addEventListener("click", (e) =>{
  setgif("trash");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

document.getElementById('slider').addEventListener("input", (e) =>{
  //ctx.clearRect(0, 0, canvas.width, canvas.height);
  lineWidth = e.target.value / 5;
  ctx.lineWidth = lineWidth;
}, false);


document.getElementById("select1").addEventListener("change", (e) => {
  console.log(e.target.value);
  fontFamily = e.target.value;
});

document.getElementById("select2").addEventListener("change", (e) => {
  console.log(e.target.value);
  
  fontSize = e.target.value;
});


window.addEventListener('popstate', (e) => {
  // https://ithelp.ithome.com.tw/articles/10195793?sc=iThelpR
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if( e.state ){
    ctx.putImageData(e.state, 0, 0);
  }
}, false);




// Color Picker https://codepen.io/amwill/pen/ZbdGeW
/*
picker.addEventListener("mousedown", (e) => {
  pick = true;
});

picker.addEventListener("mouseup", (e) => {
  pick = false;
});

picker.addEventListener("mousemove", (e) => {
  if(pick){
    let imageData = picker.getImageData(e.offsetX, e.offsetY, 1, 1).data;
    pickedColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    let colorLabel = document.getElementById('color-label');
    colorLabel.style.backgroundColor = pickedColor;
  }
});*/

picker.addEventListener("click", (e) => {
  let imageData = ptx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
  let color = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  pickedColor = color;
  
  console.log(pickedColor);
});



RGBStrip.addEventListener("click", (e) => {
  let imageData = rtx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
  let color = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  fillPicker(color);
});

function fillPicker(color = "rgba(255,0,0,1)"){
  // ctx.createLinearGradient(x0, y0, x1, y1);
  let gradient1 = ptx.createLinearGradient(0, 0, picker.width, 0); 
  gradient1.addColorStop(0, 'rgba(255,255,255,1)');
  gradient1.addColorStop(1, color);
  
  let gradient2 = ptx.createLinearGradient(0, 0, 0, picker.height);
  gradient2.addColorStop(0, 'rgba(0,0,0,0)');
  gradient2.addColorStop(1, 'rgba(0,0,0,1)');
  
  // fillRect(x, y, width, height)
  ptx.fillStyle = gradient1; // set property
  ptx.fillRect(0, 0, picker.width, picker.height); 
  ptx.fillStyle = gradient2;
  ptx.fillRect(0, 0, picker.width, picker.height);
}

function fillRGBStrip(){
   let gradient = rtx.createLinearGradient(0, 0, 0, RGBStrip.height);
   let gradientArray = ["#ff0000", "#ffff00", "#00ff00", "#00ffff", "#0000ff", "#ff00ff", "#ff0000"];

   for(let i = 0, j = 0; i<= 1; i += 0.17){
     gradient.addColorStop(i, gradientArray[j++]);
   }
  
   rtx.fillStyle = gradient;
   rtx.fillRect(0, 0, RGBStrip.width, RGBStrip.height);
}

