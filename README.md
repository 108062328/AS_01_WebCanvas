# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| 調色盤動畫                                         | 1~5%      | Y         |
| 動畫印章                                           | 1~5%      | Y         |

---

### How to use 
    Describe how to use your web and maybe insert images to help you explain.
![](https://i.imgur.com/TexXYDb.jpg)
    
這個是網頁的介面，總共有三個主要的欄位。
- 畫布，位於畫面正中間
- 調色盤，位於畫面右邊
- 工具欄，位於畫面下方


--- 

#### 調色盤
![](https://i.imgur.com/Gvwt7cl.png)

調色盤有4個功能，由上到下分別為：

- 選擇畫筆顏色
- 選擇字型
- 選擇字體大小
- 選擇畫筆大小

調色盤下方的動畫會隨著選擇的畫筆而改變。


--- 

#### 工具欄

![](https://i.imgur.com/s7RJgtC.png)

下方工具欄最左邊的七個藍色按鈕是各種畫筆
- Brush
- Eraser
- Text Input
- Circle
- Rectangle
- Triangle
- Bouns function (Animation Stamp)

中間有四個黑色的功能按鈕：
- Undo
- Redo
- Upload (Image Tool)
- Download

最右邊的是紅色的重置畫布按鈕(Refresh button)。



### Function description

    Describe your bouns function and how to use it.
    
#### 調色盤動畫

在工具欄選擇不同的工具會改變調色盤下方的動畫。
![](https://i.imgur.com/UvyDq3J.gif)
![](https://i.imgur.com/69Z0QBY.gif)


#### 動畫印章

工具欄第七個按鈕可以選擇動畫印章。可以在畫布上面畫出小阿梅(Smol Ame)的動畫。
    
![](https://i.imgur.com/fwLbzvC.gif)
![](https://i.imgur.com/0OWspRe.gif)


### Gitlab page link
https://108062328.gitlab.io/AS_01_WebCanvas/

### Others (Optional)

    Anything you want to say to TAs.

如果滑鼠游標沒有改變，可以重新整理頁面，或是在畫布上面隨便畫幾筆。




<style>
table th{
    width: 100%;
}
</style>
